class Circle {
 
    float x;
    float y;
    // the Circle radius
    float r;
 
    Circle(float xpos, float ypos, float radius) {
        x = xpos;
        y = ypos;
        r = radius;
     
     
    }
     
    void render() {
        ellipseMode(CENTER);
        ellipse(x,y,r*2, r*2);
    }            
}