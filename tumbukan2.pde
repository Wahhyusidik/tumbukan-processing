import de.looksgood.ani.*;
import de.looksgood.ani.easing.*; 
import controlP5.*;
import grafica.*;
GPlot plot;

ControlP5 cp5;
float ma = 8;
float mb = 8;
float va=4;
float vb=0;
float e = 0.93;

float vad,vbd,vna,vnb;

float ra = ma * 5;
float rb = mb * 5;
float xa = 300;
float xb = 700;
float yballs = 340;
  Circle a = new Circle(xa,yballs-ra,ra);
  Circle b = new Circle(xb,yballs-rb,rb);
boolean rectOver,rrectOver;
color rectcolor;
boolean rectclicked;
boolean acircleOver;
boolean bcircleOver;
boolean alocked;
boolean blocked;
boolean buttonpressed;
String evalue,m1,v1,m2,v2;
boolean drawgraph;


int nPoints = 100;
  GPointsArray points = new GPointsArray(nPoints);
  GPointsArray points2 = new GPointsArray(nPoints);


void setup() {
  size(1200,640);
  noStroke();
  PFont font = createFont("arial",20);
  
  cp5 = new ControlP5(this);
 /* cp5.addSlider("e")
     .setPosition(width/2-50,20)
     .setWidth(100)
     .setHeight(30)
     .setRange(0,1) // values can range from big to small as well
     .setValue(1)
     .setNumberOfTickMarks(100)
     .setSliderMode(Slider.FLEXIBLE)
     ;*/
  cp5.addTextfield("evalue")
     .setPosition(width/2-50,80-40)
     .setSize(100,40)
     .setFont(font)
     .setAutoClear(false)
     .setFocus(false)
     .setColor(color(38,38,38))
     .setColorBackground(color(204,227,245))
     .setValue("1")
     
     .getValueLabel().align(ControlP5.CENTER, ControlP5.CENTER)
   //  .getCaptionLabel().align(ControlP5.CENTER, ControlP5.BOTTOM)
  ;
  cp5.addTextfield("v1")
     .setPosition(width/2-50-20-100,80-40)
     .setSize(100,40)
     .setFont(font)
     .setAutoClear(false)
     .setFocus(false)
     .setColor(color(38,38,38))
     .setColorBackground(color(204,227,245))
     .setValue("3")
     .getValueLabel().align(ControlP5.CENTER, ControlP5.CENTER)
   //  .getCaptionLabel().align(ControlP5.CENTER, ControlP5.BOTTOM)
  ;
  cp5.addTextfield("m1")
     .setPosition(width/2-50-20-100-20-100,80-40)
     .setSize(100,40)
     .setFont(font)
     .setAutoClear(false)
     .setFocus(false)
     .setColor(color(38,38,38))
     .setColorBackground(color(204,227,245))
     .setValue("8")
     .setInputFilter(ControlP5.FLOAT)
     .getValueLabel().align(ControlP5.CENTER, ControlP5.CENTER)
   //  .getCaptionLabel().align(ControlP5.CENTER, ControlP5.BOTTOM)
  ;
  cp5.addTextfield("v2")
     .setPosition(width/2-50+20+100+20+100,80-40)
     .setSize(100,40)
     .setFont(font)
     .setAutoClear(false)
     .setFocus(false)
     .setColor(color(38,38,38))
     .setColorBackground(color(204,227,245))
     .setValue("-4")
     .getValueLabel().align(ControlP5.CENTER, ControlP5.CENTER)
   //  .getCaptionLabel().align(ControlP5.CENTER, ControlP5.BOTTOM)
  ;
  cp5.addTextfield("m2")
     .setPosition(width/2-50+100+20,80-40)
     .setSize(100,40)
     .setFont(font)
     .setAutoClear(false)
     .setFocus(false)
     .setColor(color(38,38,38))
     .setColorBackground(color(204,227,245))
     .setValue("8")
     .setInputFilter(ControlP5.FLOAT)
     .getValueLabel().align(ControlP5.CENTER, ControlP5.CENTER)
   //  .getCaptionLabel().align(ControlP5.CENTER, ControlP5.BOTTOM)
  ;
  cp5.getController("evalue").getCaptionLabel().setColor(color(38,38,38)).align(ControlP5.CENTER, ControlP5.TOP_OUTSIDE).setPaddingX(0);
  cp5.getController("m1").getCaptionLabel().setColor(color(38,38,38)).align(ControlP5.CENTER, ControlP5.TOP_OUTSIDE).setPaddingX(0);
  cp5.getController("v1").getCaptionLabel().setColor(color(38,38,38)).align(ControlP5.CENTER, ControlP5.TOP_OUTSIDE).setPaddingX(0);
  cp5.getController("m2").getCaptionLabel().setColor(color(38,38,38)).align(ControlP5.CENTER, ControlP5.TOP_OUTSIDE).setPaddingX(0);
  cp5.getController("v2").getCaptionLabel().setColor(color(38,38,38)).align(ControlP5.CENTER, ControlP5.TOP_OUTSIDE).setPaddingX(0);
  textFont(createFont("calibri", 36));
  
  /* Position
float midx=width/2;
float midy=height/2;
yballs = midy;
a.y=yballs-(a.r/2);
b.y=yballs-(b.r/2);

*/
    
  Ani.init(this);
  // Prepare the points for the plot
  /*int nPoints = 100;
  GPointsArray points = new GPointsArray(nPoints);
  GPointsArray points2 = new GPointsArray(nPoints);

  */
  
rectcolor = color(28,173,228);


 /* for(float i = 0; i<= nPoints; i+=0.01) {
    
    points.add(i, ((ma*va) + (mb*vb) + (i*ma*(va-vb)))/(ma+mb));
    points2.add(i, (-1*e*(va-vb))+((ma*va) + (mb*vb) + (i*ma*(va-vb)))/(ma+mb));
  }

  // Create a new plot and set its position on the screen
  plot = new GPlot(this,20, height-250-20,500,250);
  //plot.setPos(20, height-300);
 
  plot.addLayer("vad", points2);
  plot.getLayer("vad").setLineColor(color(28,173,228));
  plot.getLayer("vad").setPointColor(color(28,173,228));
  plot.addLayer("vbd", points);
  plot.getLayer("vbd").setLineColor(color(66,186,151));
  plot.getLayer("vbd").setPointColor(color(66,186,151));
  // or all in one go
  // GPlot plot = new GPlot(this, 25, 25);

  // Set the plot title and the axis labels
  plot.setTitleText("Grafik kecepatan akhir benda terhadap e");
  plot.getXAxis().setAxisLabelText("e (koefisien restitusi)");
  plot.getYAxis().setAxisLabelText("v akhir");
  plot.setXLim(0, 1);
  // Add the points
  plot.setPoints(points, "vbd");
  plot.setPoints(points2, "vad");
*/
  // Draw it!
  // Create a new plot and set its position on the screen
  plot = new GPlot(this,20, height-250-20,500,250);
  //plot.setPos(20, height-300);
 
  plot.addLayer("vad", points2);
  plot.getLayer("vad").setLineColor(color(28,173,228));
  plot.getLayer("vad").setPointColor(color(28,173,228));
  plot.addLayer("vbd", points);
  plot.getLayer("vbd").setLineColor(color(66,186,151));
  plot.getLayer("vbd").setPointColor(color(66,186,151));
  // Set the plot title and the axis labels
  plot.setTitleText("Grafik kecepatan akhir benda terhadap e");
  plot.getXAxis().setAxisLabelText("e (koefisien restitusi)");
  plot.getYAxis().setAxisLabelText("v akhir");
  plot.setXLim(0, 1);
  // Add the points
  
}

void draw() {

//println(evalue);
  //println(" V a akhir ",vad);
  //println(" V b akhir ",vbd);
  background(250);
  
  
  fill(0);
  if (!rectclicked){
    
  evalue=cp5.get(Textfield.class,"evalue").getText();
  if(evalue.length() == 0){
    e=1;
    //ma=float(m1);
  } else {
    e=float(evalue);
  }
  
  m1=cp5.get(Textfield.class,"m1").getText();
  if(m1.length() == 0){
    ma=0;
    //ma=float(m1);
  } else {
    ma=float(m1);
    //println("zero");
  }
  v1=cp5.get(Textfield.class,"v1").getText();
  if(v1.length() == 0){
    va=0;
    //ma=float(m1);
  } else {
    va=float(v1);
  }
  
  m2=cp5.get(Textfield.class,"m2").getText();
  if(m2.length() == 0){
    mb=0;
  } else {
    mb=float(m2);
  }
  
  v2=cp5.get(Textfield.class,"v2").getText();
  if(v2.length() == 0){
    vb=0;
  } else {
    vb=float(v2);
  }
  
  vna=va;
  vnb=vb;
  a.r=ma*5;
  b.r=mb*5;
  a.y=340-a.r;
  b.y=340-b.r;
  
  }
  a.r=ma*5;
  b.r=mb*5;
  if (buttonpressed){
    for(float i = 0; i<= nPoints; i+=0.01) {
    float vbdp = ((ma*va) + (mb*vb) + (i*ma*(va-vb)))/(ma+mb);
    float vadp = (-1*i*(va-vb))+vbdp;
    points.add(i,vbdp);
    points2.add(i,vadp);
    println(i+"v2"+vadp+"v1"+vbdp);
  }
  plot.setPoints(points, "vbd");
  plot.setPoints(points2, "vad");
  }

    plot.defaultDraw();
  
   
  ellipseMode(CENTER);
  fill(28,173,228);
  rect(0,345,width,2);
  a.render();
  fill(66,186,151);
  b.render();
  fill(rectcolor);
  rect(width-20-120,20,120,60);
  fill(28,173,228);
  rect(20,20,120,60);
  textSize(24);
  textAlign(CENTER,CENTER);
  fill(255);
  text("Play", width-20-120+60,20+30-4);
  text("Reset",20+60,20+30-4);
  rectOver=overRect(width-20-120,20,120,60);
  rrectOver=overRect(20,20,120,60);
  acircleOver=overCircle(a.x,a.y,a.r);
  bcircleOver=overCircle(b.x,b.y,b.r);
  
  fill(38,38,38);
  textAlign(CENTER,CENTER);
  text(vbd*mb, width-20-120+60,height-20-24);
  text(vb*mb, width-20-120+60-120,height-20-24);
  text(vbd, width-20-120+60-120-120,height-20-24);
  text("Hijau(2)", width-20-120+60-120-120-120,height-20-24);
  text(vad*ma, width-20-120+60,height-20-24-30);
  text(va*ma, width-20-120+60-120,height-20-24-30);
  text(vad, width-20-120+60-120-120,height-20-24-30);
  text("Biru(1)", width-20-120+60-120-120-120,height-20-24-30);
  text("P akhir", width-20-120+60,height-20-24-30-30);
  text("P awal", width-20-120+60-120,height-20-24-30-30);
  text("V akhir", width-20-120+60-120-120,height-20-24-30-30);
  text("Benda", width-20-120+60-120-120-120,height-20-24-30-30);
   /*
  if (a.x >= width - a.r){
    va*=-1;
    a.x = width - a.r;
  } else if (a.x <= 0 + a.r){
    va*=-1;
    a.x = 0 + a.r;
  }
  
  if (b.x >= width - b.r){
    vb*=-1;
    b.x = width - b.r;
  } else if (b.x <= 0 + b.r){
    vb*=-1;
    b.x = 0 + b.r;
  }
  */
  if(buttonpressed){
    vna=va;
    vnb=vb;
    a.r=ma*5;
  b.r=mb*5;
  }
  if (rectclicked == true){
    
  if(dist(a.x, a.y, b.x, b.y) <= a.r + b.r) {
    vbd = ((ma*va) + (mb*vb) + (e*ma*(va-vb)))/(ma+mb);
    vad = (-1*e*(va-vb))+vbd;
        vna=vad;
        vnb=vbd;
    }
    if (rectclicked){
      
      a.x+=vna;
      b.x+=vnb;
    } else {
      
    }
  }
 
  
  if(buttonpressed){
    rectcolor = color(0,112,192);
  } else {
    rectcolor = color(28,173,228);
  }
 
}


void mousePressed() {
  if (rectOver) {
   buttonpressed=true;
    if (rectclicked == false){
      rectclicked = true;
    } /*else if (rectclicked == false){
      rectclicked = true;
    }*/
  }
  if (rrectOver){
   ma=8;
    a.x=300;
    mb=8;
    b.x=700;
    vna=0;
    vnb=0;
    vad=0;
    vbd=0;
    rectclicked=false;
    
      // Create a new plot and set its position on the screen
  plot = new GPlot(this,20, height-250-20,500,250);
  //plot.setPos(20, height-300);
 
  plot.addLayer("vad", points2);
  plot.getLayer("vad").setLineColor(color(28,173,228));
  plot.getLayer("vad").setPointColor(color(28,173,228));
  plot.addLayer("vbd", points);
  plot.getLayer("vbd").setLineColor(color(66,186,151));
  plot.getLayer("vbd").setPointColor(color(66,186,151));
  // Set the plot title and the axis labels
  plot.setTitleText("Grafik kecepatan akhir benda terhadap e");
  plot.getXAxis().setAxisLabelText("e (koefisien restitusi)");
  plot.getYAxis().setAxisLabelText("v akhir");
  plot.setXLim(0, 1);
  points = new GPointsArray(nPoints);
  points2 = new GPointsArray(nPoints);
  plot.setPoints(points, "vbd");
  plot.setPoints(points2, "vad");
  
  }
  if(!rectclicked){
     if(acircleOver) { 
      alocked = true; 
    } else {
      alocked = false;
    }
    
    if(bcircleOver) { 
      blocked = true; 
    } else {
      blocked = false;
    }
  }
}

void mouseDragged() {
  if(alocked) {
    a.x = mouseX; 
  }
  if(blocked) {
    b.x = mouseX; 
  }
}

void mouseReleased() {
  alocked = false;
  blocked = false;
  buttonpressed=false;
}

boolean overRect(int x, int y, int width, int height)  {
  if (mouseX >= x && mouseX <= x+width && 
      mouseY >= y && mouseY <= y+height) {
    return true;
  } else {
    return false;
  }
}

boolean overCircle(float x, float y, float r) {
  float disX = x - mouseX;
  float disY = y - mouseY;
  if (sqrt(sq(disX) + sq(disY)) < r ) {
    return true;
  } else {
    return false;
  }
}